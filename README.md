## INTRODUCTION

The Structure Map module helps you to visualize the structure of your entities
and bundles. Including the basic entiry information, editorial permissions,
display modes, field settings, etc.

The primary use case for this module is to provide a simple way to understand
a site architecture. This is specially helpful when you have a complex site
with many entities and bundles and you need to capture the current architecture
of the site.

## REQUIREMENTS

This module depends on the following modules:

- Configuration Manager
- Entity
- Field
- System
- Field UI
- Menu UI
- Custom Menu Links
- Link
- User

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

- This module doesn't require any configuration.

## USAGE

- Go to `/admin/structure/map` and select an entity type and bundle.
- The module will generate a structure map for the selected values.
- If you want to export multiple entities and bundles you can do it
going to `/admin/structure/map/export`. The data will be exported to an
excel file.

## MAINTAINERS

Current maintainers:

- ALVARO DE MENDOZA - https://www.drupal.org/u/alvarodemendoza
