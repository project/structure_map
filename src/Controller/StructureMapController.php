<?php

namespace Drupal\structure_map\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Session\AccountInterface;
use Drupal\structure_map\EntityTypeInfo;
use Drupal\structure_map\StructureMapTable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides methods to handle and display structure maps.
 */
class StructureMapController extends ControllerBase {

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The widget plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $widgetPluginManager;

  /**
   * The role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new StructureMapController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle info service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Plugin\DefaultPluginManager $widget_plugin_manager
   *   The widget plugin manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigManagerInterface $config_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    EntityTypeBundleInfoInterface $bundle_info,
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityFieldManagerInterface $entity_field_manager,
    DefaultPluginManager $widget_plugin_manager,
    RequestStack $request_stack,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configManager = $config_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->bundleInfo = $bundle_info;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityFieldManager = $entity_field_manager;
    $this->widgetPluginManager = $widget_plugin_manager;
    $this->roleStorage = $entity_type_manager->getStorage('user_role');
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_display.repository'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.widget'),
      $container->get('request_stack'),
    );
  }

  /**
   * Handles the entity and bundle.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   *
   * @return array
   *   A render array.
   */
  public function entityBundle($entity_type, $bundle) {
    // Load the entity type definition.
    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type);
    // Get the human-readable label for the entity type.
    $entity_type_label = $entity_type_definition->getLabel();

    // Load the bundle information.
    $bundle_info = $this->bundleInfo->getBundleInfo($entity_type);
    // Get the human-readable label for the bundle.
    $bundle_label = $bundle_info[$bundle]['label'];

    // Instantiate EntityTypeInfo with all required dependencies.
    $entity_type_info = new EntityTypeInfo(
      $this->entityTypeManager,
      $this->configManager,
      $this->fieldTypeManager,
      $this->entityDisplayRepository,
      $this->entityFieldManager,
      $this->widgetPluginManager,
      $this->roleStorage,
      $this->bundleInfo,
      $this->requestStack,
    );

    // Check if 'show_relationship_information' exists and is true.
    $show_relationship_information = $this->requestStack->getCurrentRequest()->query->get('show_relationship_information') === 'true';
    // Check if 'show_hidden_fields' exists and is true.
    $show_hidden_fields = $this->requestStack->getCurrentRequest()->query->get('show_hidden_fields') === 'true';

    // Get basic entity info.
    $basic_entity_type_info = $entity_type_info->getBasicEntityInfo($entity_type, $bundle, $show_relationship_information, $show_hidden_fields);
    // Get basic field entity info.
    $basic_field_entity_info = $entity_type_info->getBasicFieldEntityInfo($entity_type, $bundle);
    // Get form display info.
    $display_info = $entity_type_info->getFormDisplayInfo($entity_type, $bundle);
    // Get view display info.
    $view_display_info = $entity_type_info->getViewDisplayInfo($entity_type, $bundle);

    // Instantiate StructureMapTable.
    $structure_map_table = new StructureMapTable();
    // Generate the basic entity information table.
    $entity_basic_information_table = $structure_map_table->generateEntityBasicInfoTable($basic_entity_type_info, $show_relationship_information);
    // Generate the basic field entity information table.
    $basic_field_entity_information_table = $structure_map_table->generateBasicFieldEntityInfoTable($basic_field_entity_info);
    // Generate the form display info table.
    $form_display_info_table = $structure_map_table->generateFormDisplayTables($display_info, $show_hidden_fields);
    // Generate the view display info table.
    $view_display_info_table = $structure_map_table->generateViewDisplayTables($view_display_info, $show_hidden_fields);

    return [
      '#type' => 'container',
      '#attributes' => ['class' => ['structure-map']],
      'header' => [
        '#markup' => $this->t('<h2>@entity_type_label: @bundle_label</h2>', [
          '@entity_type_label' => $entity_type_label,
          '@bundle_label' => $bundle_label,
        ]) . '<br>' . $this->t('Viewing information for entity type: <strong>@entity_type</strong>, bundle: <strong>@bundle</strong>', [
          '@entity_type' => $entity_type,
          '@bundle' => $bundle,
        ]),
      ],
      'basic_info' => [
        '#markup' => '<h3>' . $this->t('Basic Information') . '</h3>',
        'table' => $entity_basic_information_table,
      ],
      'basic_field_info' => [
        '#markup' => '<h3>' . $this->t('Fields') . '</h3>',
        'table' => $basic_field_entity_information_table,
      ],
      'form_display_info' => [
        '#markup' => '<h3>' . $this->t('Form Display') . '</h3>',
        'table' => $form_display_info_table,
      ],
      'view_display_info' => [
        '#markup' => '<h3>' . $this->t('View Display') . '</h3>',
        'table' => $view_display_info_table,
      ],
      '#attached' => [
        'library' => [
          'structure_map/structure_map_styles',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access administration pages');
  }

}
