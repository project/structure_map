<?php

namespace Drupal\structure_map;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;

/**
 * Class to get information about an entity type.
 */
class EntityTypeInfo {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The entity form display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $formDisplayStorage;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field widget plugin manager.
   *
   * @var \Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $widgetPluginManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfo;

  /**
   * Constructs a new EntityTypeInfo object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Plugin\DefaultPluginManager $widget_plugin_manager
   *   The field widget plugin manager.
   * @param \Drupal\Core\Entity\EntityStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigManagerInterface $config_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityFieldManagerInterface $entity_field_manager,
    DefaultPluginManager $widget_plugin_manager,
    EntityStorageInterface $role_storage,
    EntityTypeBundleInfoInterface $bundle_info,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configManager = $config_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->formDisplayStorage = $entity_type_manager->getStorage('entity_form_display');
    $this->entityFieldManager = $entity_field_manager;
    $this->widgetPluginManager = $widget_plugin_manager;
    $this->fieldTypeManager = $field_type_manager;
    $this->roleStorage = $role_storage;
    $this->bundleInfo = $bundle_info;
  }

  /**
   * Retrieves the basic information of an entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type machine name.
   * @param string $bundle
   *   The bundle machine name.
   * @param bool $show_relationship_information
   *   Whether to show relationship information.
   *
   * @return array
   *   An array containing the basic information.
   */
  public function getBasicEntityInfo(string $entity_type, string $bundle, bool $show_relationship_information = FALSE): array {
    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type);
    $bundle_name = $this->bundleInfo->getBundleInfo($entity_type)[$bundle]['label'];
    $bundle_machine_name = $bundle;
    // Entity and bundle names.
    $entity_name = $entity_type_definition->getLabel()->__toString();
    $entity_machine_name = $entity_type_definition->id();

    // Title field machine name.
    $title_field_machine_name = $entity_type_definition->get('entity_keys')['label'];

    // Get the base field definitions for the entity type.
    $base_field_definitions = $this->entityFieldManager->getBaseFieldDefinitions($entity_type);

    // Retrieve the human-readable label for the title field.
    $title_field_label = '';
    if (isset($base_field_definitions[$title_field_machine_name])) {
      $title_field_label = $base_field_definitions[$title_field_machine_name]->getLabel()->__toString();
    }

    // Check if the 'type' field exists.
    $has_type_field = isset($base_field_definitions['type']);

    // Count the total number of entity bundle items.
    $entity_storage = $this->entityTypeManager->getStorage($entity_type);
    $query = $entity_storage->getQuery()->accessCheck(TRUE);
    if ($has_type_field) {
      $query->condition('type', $bundle);
    }
    $total_count = $query->count()->execute();

    if ($show_relationship_information) {
      // Summarize how many times this entity bundle has been referenced.
      $reference_info = [];
      $all_field_definitions = $this->entityFieldManager->getFieldMap();
      foreach ($all_field_definitions as $referencing_entity_type => $fields) {
        foreach ($fields as $field_name => $field_info) {
          if (isset($field_info['type']) && $field_info['type'] === 'entity_reference') {
            foreach ($field_info['bundles'] as $referencing_bundle) {
              $field_definitions = $this->entityFieldManager->getFieldDefinitions($referencing_entity_type, $referencing_bundle);
              if (isset($field_definitions[$field_name])) {
                $field_definition = $field_definitions[$field_name];
                $settings = $field_definition->getSettings();
                if (isset($settings['target_type']) && $settings['target_type'] === $entity_type) {
                  if (isset($settings['handler_settings']['target_bundles']) && in_array($bundle, $settings['handler_settings']['target_bundles'])) {
                    $referencing_entity_storage = $this->entityTypeManager->getStorage($referencing_entity_type);
                    $target_ids = $entity_storage->getQuery()->accessCheck(TRUE)->execute();
                    $reference_query = $referencing_entity_storage->getQuery()
                      ->condition($field_name . '.target_id', $target_ids, 'IN')
                      ->accessCheck(TRUE);
                    $count = $reference_query->count()->execute();
                    if ($count > 0 && $show_relationship_information) {
                      $referencing_entity_type_definition = $this->entityTypeManager->getDefinition($referencing_entity_type);
                      $referencing_entity_type_label = $referencing_entity_type_definition->getLabel()->__toString();
                      $bundle_label = $this->bundleInfo->getBundleInfo($referencing_entity_type)[$referencing_bundle]['label'];
                      $reference_info["{$referencing_entity_type}__{$referencing_bundle}"] = [
                        'label' => $referencing_entity_type_label . ' ' . $bundle_label,
                        'count' => $count,
                      ];
                    }
                  }
                }
              }
            }
          }
        }
      }
      // Convert reference info to string.
      $reference_info_string = '';
      foreach ($reference_info as $info) {
        $reference_info_string .= "{$info['label']}: {$info['count']} <br/>";
      }
    }

    // Check which roles have edit and view access to this entity.
    $roles = $this->roleStorage->loadMultiple();
    $edit_any_access_roles = [];
    $edit_own_access_roles = [];

    // Normalize entity type for nodes.
    if ($entity_type === 'node') {
      $entity_type = 'content';
    }

    foreach ($roles as $role) {
      $edit_permission_any = 'edit any ' . $bundle . ' ' . $entity_type;
      $edit_permission_own = 'edit own ' . $bundle . ' ' . $entity_type;

      if ($role->hasPermission($edit_permission_any)) {
        $edit_any_access_roles[] = $role->label();
      }
      if ($role->hasPermission($edit_permission_own)) {
        $edit_own_access_roles[] = $role->label();
      }
    }

    return [
      'entity_name' => $entity_name,
      'entity_machine_name' => $entity_machine_name,
      'bundle_name' => $bundle_name,
      'bundle_machine_name' => $bundle_machine_name,
      'title_field_label' => $title_field_label,
      'edit_own_access' => $edit_own_access_roles,
      'edit_any_access' => $edit_any_access_roles,
      'total_count' => $total_count,
      'reference_info' => $reference_info_string ?? '',
    ];
  }

  /**
   * Retrieves the fields information of an entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type machine name.
   * @param string $bundle
   *   The bundle machine name.
   *
   * @return array
   *   An array containing the fields information.
   */
  public function getBasicFieldEntityInfo(string $entity_type, string $bundle): array {
    $fields_info = [];
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

    foreach ($field_definitions as $field_name => $field_definition) {

      // Skip base fields and only include configurable fields.
      if ($field_definition instanceof FieldConfig) {

        // Load the field configuration.
        $field_config = FieldConfig::loadByName($entity_type, $bundle, $field_name);
        /** @var \Drupal\field\FieldConfigInterface $field_config */
        $field_storage = $field_config->getFieldStorageDefinition();

        // Load the field summary.
        $storage_summary = $this->fieldTypeManager->getStorageSettingsSummary($field_storage);
        $instance_summary = $this->fieldTypeManager->getFieldSettingsSummary($field_config);
        $summary_list = [...$storage_summary, ...$instance_summary];
        $field_type_summary = implode('<br/>', $summary_list);

        // Get the field type label.
        $field_type = $this->fieldTypeManager->getDefinitions()[$field_storage->getType()];
        $field_type_label = $field_type['label'];

        // Count the number of entities using a reference field.
        $entity_count = 0;
        if ($field_storage->getType() === 'entity_reference') {
          $entity_storage = $this->entityTypeManager->getStorage($entity_type);
          if ($entity_type == 'media') {
            $query = $entity_storage->getQuery()
              ->condition('bundle', $bundle)
              ->exists($field_name)
              ->accessCheck(TRUE);
          }
          else {
            $query = $entity_storage->getQuery()
              ->condition('type', $bundle)
              ->exists($field_name)
              ->accessCheck(TRUE);
          }
          $entity_count = $query->count()->execute();

          // Add the information to the summary.
          $field_type_summary .= '<br/>' . $this->t('Number of entities using this reference field: @count', ['@count' => $entity_count]);
        }

        $label = $field_definition->getLabel();
        $field_info = [
          'label' => $label,
          'machine_name' => $field_name,
          'field_type' => $field_type_label,
          'required' => $field_definition->isRequired() ? 'Yes' : 'No',
          'cardinality' => $field_definition->getFieldStorageDefinition()->getCardinality() === -1 ? 'Unlimited' : $field_definition->getFieldStorageDefinition()->getCardinality(),
          'default_value' => $field_definition->getDefaultValueLiteral(),
          'settings' => $field_type_summary,
        ];
        $fields_info[] = $field_info;
      }
    }

    return $fields_info;
  }

  /**
   * Retrieves the form display information of an entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type machine name.
   * @param string $bundle
   *   The bundle machine name.
   *
   * @return array
   *   An array containing the form display information.
   */
  public function getFormDisplayInfo(string $entity_type, string $bundle): array {
    $entity_display_repository = $this->entityDisplayRepository;
    $form_display_storage = $this->formDisplayStorage;
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $widget_plugin_manager = $this->widgetPluginManager;

    // Create an array with sto store the information for the different modes.
    $form_display_info = [];

    // Get the form modes.
    $form_modes = $entity_display_repository->getFormModes($entity_type);
    // Add the default form mode to the form modes array.
    $form_modes['default'] = ['label' => 'Default'];

    foreach ($form_modes as $form_mode => $form_mode_info) {

      // Load the form display for the current form mode.
      $form_display = $form_display_storage->load($entity_type . '.' . $bundle . '.' . $form_mode);
      if (!$form_display) {
        continue;
      }

      // Set the form mode label.
      $form_display_info[$form_mode]['label'] = $form_mode_info['label'];

      if ($form_display) {
        // Get a list of hidden fields.
        $hidden_fields = [];
        foreach ((array) $form_display as $key => $value) {
          if (strpos($key, 'hidden') !== FALSE) {
            $hidden_fields = array_keys($value);
          }
        }

        // Get the disabled fields.
        foreach ($hidden_fields as $key => $field_name) {
          // Get the field label.
          $field_label = isset($field_definitions[$field_name]) ? $field_definitions[$field_name]->getLabel() : $field_name;

          $form_display_info[$form_mode]['disabled'][] = [
            'field_label' => $field_label,
            'machine_name' => $field_name,
          ];
        }

        // Get the enabled fields.
        foreach ($form_display->getComponents() as $field_name => $component) {
          // Get the field label.
          $field_label = isset($field_definitions[$field_name]) ? $field_definitions[$field_name]->getLabel() : $field_name;

          // Get the widget name.
          $widget = $component['type'] ?? NULL;

          // Check if the widget type is valid.
          if (!empty($widget)) {
            // Get the human-readable name of the widget.
            $widget_definition = $widget_plugin_manager->getDefinition($widget);
            $widget_label = $widget_definition['label'];

            // Get the widget summary.
            $plugin = $form_display->getRenderer($field_name);
            if ($plugin) {
              $widget_settings = $plugin->settingsSummary();
              $widget_settings_summary = implode('<br/>', $widget_settings);
            }
            else {
              $widget_settings_summary = '';
            }
          }

          // Get the field group if exists.
          $field_group = $component['region'] ?? NULL;

          $form_display_info[$form_mode]['enabled'][] = [
            'field_label' => $field_label,
            'machine_name' => $field_name,
            'widget_name' => $widget,
            'widget_label' => $widget_label ?? '',
            'widget_configuration' => $widget_settings_summary ?? '',
            'field_group' => $field_group,
          ];
        }
      }
    }
    return $form_display_info;
  }

  /**
   * Retrieves the view display information of an entity type and bundle.
   *
   * @param string $entity_type
   *   The entity type machine name.
   * @param string $bundle
   *   The bundle machine name.
   *
   * @return array
   *   An array containing the form display information.
   */
  public function getViewDisplayInfo(string $entity_type, string $bundle): array {
    $entity_display_repository = $this->entityDisplayRepository;
    $view_display_storage = $this->entityTypeManager->getStorage('entity_view_display');
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);

    // Create an array to store the information for the different modes.
    $view_display_info = [];

    // Get the view modes.
    $view_modes = $entity_display_repository->getViewModes($entity_type);
    // Add the default view mode to the view modes array.
    $view_modes['default'] = ['label' => 'Default'];

    foreach ($view_modes as $view_mode => $view_mode_info) {

      // Load the view display for the current view mode.
      $view_display = $view_display_storage->load($entity_type . '.' . $bundle . '.' . $view_mode);
      if (!$view_display) {
        continue;
      }

      // Set the view mode label.
      $view_display_info[$view_mode]['label'] = $view_mode_info['label'];

      if ($view_display) {
        // Get a list of hidden fields.
        $hidden_fields = [];
        foreach ((array) $view_display as $key => $value) {
          if (strpos($key, 'hidden') !== FALSE) {
            $hidden_fields = array_keys($value);
          }
        }

        // Get the disabled fields.
        foreach ($hidden_fields as $field_name) {
          // Get the field label.
          $field_label = isset($field_definitions[$field_name]) ? $field_definitions[$field_name]->getLabel() : $field_name;

          $view_display_info[$view_mode]['disabled'][] = [
            'field_label' => $field_label,
            'machine_name' => $field_name,
          ];
        }

        // Get the enabled fields.
        foreach ($view_display->getComponents() as $field_name => $component) {
          // Get the field label.
          $field_label = isset($field_definitions[$field_name]) ? $field_definitions[$field_name]->getLabel() : $field_name;

          // Get the label settings.
          $label_settings = $component['label'] ?? NULL;

          // Get the format and format settings.
          $format = $component['type'] ?? NULL;

          // Get the widget summary.
          $plugin = $view_display->getRenderer($field_name);
          if ($plugin) {
            $widget_settings = $plugin->settingsSummary();
            $widget_settings_summary = implode('<br/>', $widget_settings);
          }
          else {
            $widget_settings_summary = '';
          }

          $view_display_info[$view_mode]['enabled'][] = [
            'field_label' => $field_label,
            'machine_name' => $field_name,
            'label_settings' => $label_settings,
            'format' => $format,
            'format_settings' => $widget_settings_summary,
          ];
        }
      }
    }
    return $view_display_info;
  }

}
