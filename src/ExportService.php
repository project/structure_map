<?php

declare(strict_types=1);

namespace Drupal\structure_map;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Export service.
 */
final class ExportService {

  use StringTranslationTrait;

  /**
   * The role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $roleStorage;

  /**
   * Constructs an ExportService object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ConfigManagerInterface $configManager,
    private readonly FieldTypePluginManagerInterface $fieldTypeManager,
    private readonly EntityTypeBundleInfoInterface $bundleInfo,
    private readonly EntityDisplayRepositoryInterface $entityDisplayRepository,
    private readonly EntityFieldManagerInterface $entityFieldManager,
    private readonly WidgetPluginManager $widgetPluginManager,
    private readonly StructureMapExportFactory $structureMapExportFactory,
  ) {
    $this->roleStorage = $entityTypeManager->getStorage('user_role');
  }

  /**
   * Build entity bundle information.
   */
  public function buildEntityBundleInformation($entity_type, $bundle, $show_relationship_information = FALSE, $show_hidden_fields = FALSE): array {
    // Load the entity type definition.
    $entity_type_definition = $this->entityTypeManager->getDefinition($entity_type);
    // Get the human-readable label for the entity type.
    $entity_type_label = $entity_type_definition->getLabel();

    // Load the bundle information.
    $bundle_info = $this->bundleInfo->getBundleInfo($entity_type);
    // Get the human-readable label for the bundle.
    $bundle_label = $bundle_info[$bundle]['label'];

    // Instantiate EntityTypeInfo with all required dependencies.
    $entity_type_info = new EntityTypeInfo(
      $this->entityTypeManager,
      $this->configManager,
      $this->fieldTypeManager,
      $this->entityDisplayRepository,
      $this->entityFieldManager,
      $this->widgetPluginManager,
      $this->roleStorage,
      $this->bundleInfo,
    );

    // Get basic entity info.
    $basic_entity_type_info = $entity_type_info->getBasicEntityInfo($entity_type, $bundle, $show_relationship_information);
    // Get basic field entity info.
    $basic_field_entity_info = $entity_type_info->getBasicFieldEntityInfo($entity_type, $bundle);
    // Get form display info.
    $display_info = $entity_type_info->getFormDisplayInfo($entity_type, $bundle);
    // Get view display info.
    $view_display_info = $entity_type_info->getViewDisplayInfo($entity_type, $bundle);

    // Instantiate StructureMapTable.
    $structure_map_table = new StructureMapExport();
    // Generate the basic entity information table.
    $entity_basic_information_table = $structure_map_table->generateEntityBasicInfoTable($basic_entity_type_info, $show_relationship_information);
    // Generate the basic field entity information table.
    $basic_field_entity_information_table = $structure_map_table->generateBasicFieldEntityInfoTable($basic_field_entity_info);
    // Generate the form display info table.
    $form_display_info_table = $structure_map_table->generateFormDisplayTables($display_info, $show_hidden_fields);
    // Generate the view display info table.
    $view_display_info_table = $structure_map_table->generateViewDisplayTables($view_display_info, $show_hidden_fields);

    return [
      'title' => $entity_type_label . ': ' . $bundle_label,
      'data' => [
        'basic_info' => [
          'title' => $this->t('Basic Information'),
          'table' => $entity_basic_information_table,
        ],
        'basic_field_info' => [
          'title' => $this->t('Fields'),
          'table' => $basic_field_entity_information_table,
        ],
        'form_display_info' => [
          'title' => $this->t('Form Display'),
          'table' => $form_display_info_table,
        ],
        'view_display_info' => [
          'title' => $this->t('View Display'),
          'table' => $view_display_info_table,
        ],
      ],
    ];
  }

  /**
   * Create and fill in the sheet for a specific entity and bundle.
   */
  public function createSheet($spreadsheet, $entity_type, $bundle, $show_relationship_information, $show_hidden_fields) {
    $sheet = $spreadsheet->createSheet();
    foreach (range('A', 'Z') as $col) {
      // 160pt converted to width (Excel width unit).
      $sheet->getColumnDimension($col)->setWidth(160 / 7);
    }
    // Enable text wrapping for all cells.
    $sheet->getStyle('A1:Z500')->getAlignment()->setWrapText(TRUE);

    $structureMapExport = $this->structureMapExportFactory->get();
    $data = $structureMapExport->buildEntityBundleInformation($entity_type, $bundle, $show_relationship_information, $show_hidden_fields);
    $sheet->setTitle(substr($entity_type . '_' . $bundle, 0, 31));
    $sheet->setCellValue('A1', $data['title']);
    $sheet->getStyle('A1')->applyFromArray([
      'font' => [
        'bold' => TRUE,
        'size' => 18,
      ],
    ]);

    $sheet->setCellValue('A3', $this->t('Viewing information for entity type: @entity_type, bundle: @bundle', [
      '@entity_type' => $entity_type,
      '@bundle' => $bundle,
    ]));
    $sheet->getStyle('A3')->applyFromArray([
      'font' => [
        'bold' => TRUE,
        'size' => 14,
      ],
    ]);
    $sheet->mergeCells('A3:C3');

    $startRow = 5;

    foreach ($data['data'] as $value) {
      $sheet->setCellValue('A' . $startRow, $value['title']);
      $sheet->getStyle('A' . $startRow)->applyFromArray([
        'font' => [
          'bold' => TRUE,
          'size' => 16,
        ],
      ]);
      $startRow++;

      foreach ($value['table'] as $table_value) {
        // Check if there is a table caption
        // (e.g. Display Mode: Default, Display Mode: Teaser, etc.).
        if (isset($table_value['#caption'])) {
          $startRow++;
          $sheet->setCellValue('A' . $startRow, $table_value['#caption']);
          $sheet->getStyle('A' . $startRow)->applyFromArray([
            'font' => [
              'bold' => TRUE,
              'size' => 14,
            ],
          ]);
          $startRow++;
        }

        // Write the header row.
        $sheet->fromArray($table_value['#header'], NULL, 'A' . $startRow);
        // Apply style to the whole row.
        $sheet->getStyle($startRow . ':' . $startRow)->applyFromArray([
          'font' => [
            'bold' => TRUE,
            'size' => 12,
          ],
        ]);

        $startRow++;
        foreach ($table_value['#rows'] as $row) {
          $sheet->fromArray($row, NULL, 'A' . $startRow);
          $startRow++;
        }
      }
      $startRow++;
    }

  }

}
