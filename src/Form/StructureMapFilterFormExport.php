<?php

namespace Drupal\structure_map\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to export structure maps based on selected entity and bundle.
 */
class StructureMapFilterFormExport extends FormBase {

  /**
   * Drupal\Core\Entity\EntityFieldManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityTypeBundleInfoInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Drupal\Core\Routing\RouteMatchInterface definition.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs the StructureMapFilterForm.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The EntityFieldManagerInterface service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityFieldManagerInterface service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The EntityTypeBundleInfoInterface service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route matcher service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, RouteMatchInterface $route_match, FileSystemInterface $file_system) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->routeMatch = $route_match;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('current_route_match'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'structure_map_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Step 1: Define the content entity checkboxes.
    $options = $this->getContentEntityOptions();
    $form['entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity'),
      '#description' => $this->t('Select starting entity'),
      '#options' => $options,
      '#ajax' => [
        'callback' => '::updateBundleCheckboxes',
        // Or TRUE to prevent re-focusing on the triggering element.
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'bundle-checkboxes-wrapper',
      ],
    ];

    // Step 2: Fetch selected entities from form state.
    $selected_entities = array_filter($form_state->getValue('entities', []));
    $form['bundle_checkboxes'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'bundle-checkboxes-wrapper'],
      '#tree' => TRUE,
    ];

    // Step 3: Dynamically add bundle checkboxes.
    foreach ($selected_entities as $entity_id) {
      $bundles = $this->getBundlesForEntity($entity_id);
      $form['bundle_checkboxes'][$entity_id] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Bundles for entity @entity', ['@entity' => $options[$entity_id]]),
        '#options' => $bundles,
      ];
    }

    $relationship_information_description = $this->t('Check to show the number of each entity bundles that reference the selected entity. As this will check all the entities, this can take a while for large sites.');
    $form['show_relationship_information'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Relationship Information'),
      '#description' => $relationship_information_description,
      '#default_value' => FALSE,
    ];

    $form['show_hidden_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Hidden Fields'),
      '#description' => $this->t(
        'Check to show hidden fields in Form and View displays.'
      ),
      '#default_value' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#prefix' => '<div id="submit_button">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * Get content entity options.
   */
  private function getContentEntityOptions() {
    $entity_types = $this->entityTypeManager->getDefinitions();
    $content_entity_types = [];
    foreach ($entity_types as $entity_type) {
      if ($entity_type->entityClassImplements(FieldableEntityInterface::class)) {
        $content_entity_types[$entity_type->id()] = $entity_type->getLabel();
      }
    }
    asort($content_entity_types);
    return $content_entity_types;
  }

  /**
   * Get bundles for a specific content entity.
   */
  private function getBundlesForEntity($entity_type_id) {
    $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
    $bundles = [];
    foreach ($bundle_info as $bundle_id => $bundle) {
      $bundles[$bundle_id] = $bundle['label'];
    }
    return $bundles;
  }

  /**
   * AJAX callback to update bundle checkboxes.
   */
  public function updateBundleCheckboxes(array &$form, FormStateInterface $form_state) {
    // Ensure the form is rebuilt.
    $form_state->setRebuild();
    return $form['bundle_checkboxes'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entities = array_filter($form_state->getValue('entities'));
    $bundles = [];
    foreach ($entities as $entity_id) {
      $bundles[$entity_id] = array_filter($form_state->getValue(['bundle_checkboxes', $entity_id], []));
    }

    $show_relationship_information = (bool) $form_state->getValue('show_relationship_information');
    $show_hidden_fields = (bool) $form_state->getValue('show_hidden_fields');

    $batch = $this->generateBatch($bundles, $show_relationship_information, $show_hidden_fields);
    batch_set($batch);
  }

  /**
   * Generate batch for exporting structure maps.
   */
  private function generateBatch($bundles, $show_relationship_information, $show_hidden_fields) {
    $operations = [];

    // We need to create a spreadsheet file here and pass the file path to the
    // batch operation because the batch update function serializes all the data
    // in the operations array and we can't pass the spreadsheet object because
    // that object can't be serialized.
    $spreadsheet = new Spreadsheet();
    $file_path = 'public://structure_map_export_' . date('Y-m-d') . '.xlsx';

    // We need an absolute path for the batch operation.
    $absolute_path = $this->fileSystem->realpath($file_path);

    $writer = new Xlsx($spreadsheet);
    $writer->save($absolute_path);

    foreach ($bundles as $entity_id => $selected_bundles) {
      foreach ($selected_bundles as $bundle_id) {
        $operations[] = [
          'structure_map_export_batch',
          [$absolute_path, $entity_id, $bundle_id, $show_relationship_information, $show_hidden_fields],
        ];
      }
    }

    return [
      'title' => $this->t('Preparing data for export.'),
      'operations' => $operations,
      'finished' => 'structure_map_export_batch_finished',
    ];
  }

}
