<?php

namespace Drupal\structure_map;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Class to generate the table structure and filters.
 */
class StructureMapExport extends StructureMapTable {

  /**
   * Generates an HTML table from the provided array.
   *
   * @param array $data
   *   The data array to generate the table from.
   * @param bool $show_relationship_information
   *   Whether to show relationship information.
   *
   * @return array
   *   The render array for the table.
   */
  public function generateEntityBasicInfoTable(array $data, bool $show_relationship_information = FALSE) {
    $header = [
      'Name',
      'Machine Name',
      'Bundle',
      'Bundle Machine Name',
      'Title Label',
      'Edit Own',
      'Edit Any',
      'Total Count',
    ];

    if ($show_relationship_information) {
      $header[] = 'Referenced By';
    }

    $row_data = [
      htmlspecialchars($data['entity_name'] ?? ''),
      htmlspecialchars($data['entity_machine_name'] ?? ''),
      htmlspecialchars($data['bundle_name'] ?? ''),
      htmlspecialchars($data['bundle_machine_name'] ?? ''),
      htmlspecialchars($data['title_field_label'] ?? ''),
      htmlspecialchars(implode(', ', $data['edit_own_access']) ?? ''),
      htmlspecialchars(implode(', ', $data['edit_any_access']) ?? ''),
      htmlspecialchars($data['total_count'] ?? ''),
    ];

    if ($show_relationship_information) {
      $row_data[] = new FormattableMarkup($data['reference_info'] ?? '', []);
    }

    $rows = [$row_data];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * Generates a table from the provided basic field entity info array.
   *
   * @param array $data
   *   The basic field entity info array to generate the table from.
   *
   * @return array
   *   The render array for the table.
   */
  public function generateBasicFieldEntityInfoTable(array $data) {
    $header = [
      'Label',
      'Machine Name',
      'Field Type',
      'Required',
      'Cardinality',
      'Default value',
      'Settings',
    ];

    $rows = [];

    foreach ($data as $field_info) {
      $default_value = $field_info['default_value'];

      // Check if the field is an entity reference.
      if (is_array($default_value) && isset($field_info['field_type']) && $field_info['field_type'] === 'entity_reference') {
        $entity_labels = [];
        foreach ($default_value as $entity_id) {
          $entity = \Drupal::entityTypeManager()->getStorage('entity_type')->load($entity_id);
          if ($entity) {
            $entity_labels[] = $entity->label();
          }
        }
        $default_value = implode(', ', $entity_labels);
      }
      else {
        if (is_array($default_value) && isset($default_value[0]['value'])) {
          if (!empty($default_value)) {
            $default_value = $default_value[0]['value'];
          }
          else {
            $default_value = '';
          }
        }
        else {
          $default_value = $default_value;
        }
      }
      if (is_array($default_value)) {
        $default_value = '';
      }

      $rows[] = [
        htmlspecialchars($field_info['label'] ?? ''),
        htmlspecialchars($field_info['machine_name'] ?? ''),
        htmlspecialchars($field_info['field_type'] ?? ''),
        htmlspecialchars($field_info['required'] ?? ''),
        htmlspecialchars($field_info['cardinality'] ?? ''),
        htmlspecialchars($default_value ?? ''),
        isset($field_info['settings']) ? str_replace('<br/>', ' / ', $field_info['settings']) : '',
      ];
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public function generateFormDisplayTables(array $data, bool $show_hidden_fields = FALSE) {
    $build = [];

    foreach ($data as $display_mode => $info) {
      $header = [
        'Label',
        'Machine Name',
        'Field Group',
        'Widget Label',
        'Widget Machine Name',
        'Widget Configuration',
      ];

      $rows = [];
      if (!empty($info['enabled'])) {
        foreach ($info['enabled'] as $field) {
          $rows[] = [
            htmlspecialchars($field['field_label'] ?? ''),
            htmlspecialchars($field['machine_name'] ?? ''),
            htmlspecialchars($field['field_group'] ?? ''),
            htmlspecialchars($field['widget_label'] ?? ''),
            htmlspecialchars($field['widget_name'] ?? ''),
            isset($field['widget_configuration']) ? str_replace('<br/>', ', ', $field['widget_configuration']) : '',
          ];
        }
      }
      if ($show_hidden_fields) {
        if (!empty($info['disabled'])) {
          foreach ($info['disabled'] as $field) {
            $rows[] = [
              htmlspecialchars($field['field_label'] ?? ''),
              htmlspecialchars($field['machine_name'] ?? ''),
            ];
          }
        }
      }

      $build['table_' . $display_mode] = [
        '#caption' => 'Display Mode: ' . htmlspecialchars($info['label']),
        '#header' => $header,
        '#rows' => $rows,
      ];
    }

    return $build;
  }

  /**
   * Generates a table for view display modes using Drupal's render arrays.
   *
   * @param array $data
   *   The data array containing view display modes.
   * @param bool $show_hidden_fields
   *   Whether to show hidden fields.
   *
   * @return array
   *   The render array for the tables.
   */
  public function generateViewDisplayTables(array $data, bool $show_hidden_fields = FALSE) {
    $build = [];

    foreach ($data as $display_mode => $info) {
      $header = [
        'Label',
        'Machine Name',
        'Label Display',
        'Format',
        'Format Settings',
      ];

      $rows = [];
      if (!empty($info['enabled'])) {
        foreach ($info['enabled'] as $field) {
          $rows[] = [
            htmlspecialchars($field['field_label'] ?? ''),
            htmlspecialchars($field['machine_name'] ?? ''),
            htmlspecialchars($field['label_settings'] ?? ''),
            htmlspecialchars($field['format'] ?? ''),
            isset($field['format_settings']) ? str_replace('<br/>', ', ', $field['format_settings']) : '',
          ];
        }
      }

      if ($show_hidden_fields) {
        if (!empty($info['disabled'])) {
          foreach ($info['disabled'] as $field) {
            $rows[] = [
              htmlspecialchars($field['field_label'] ?? ''),
              htmlspecialchars($field['machine_name'] ?? ''),
            ];
          }
        }
      }

      $build['table_' . $display_mode] = [
        '#type' => 'table',
        '#caption' => 'Display Mode: ' . htmlspecialchars($info['label']),
        '#header' => $header,
        '#rows' => $rows,
      ];
    }

    return $build;
  }

}
