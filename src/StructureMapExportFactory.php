<?php

namespace Drupal\structure_map;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Factory class for creating StructureMapExport instances.
 *
 * This class provides a method to retrieve the structure_map.export service
 * from the container.
 */
class StructureMapExportFactory {

  /**
   * The container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Constructs a new StructureMapExportFactory.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * Gets the structure_map.export service.
   *
   * @return \Drupal\structure_map\StructureMapExport
   *   The structure_map.export service.
   */
  public function get() {
    return $this->container->get('structure_map.export');
  }

}
