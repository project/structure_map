<?php

namespace Drupal\structure_map;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class to generate the table structure and filters.
 */
class StructureMapTable {
  use StringTranslationTrait;

  /**
   * Generates an HTML table from the provided array.
   *
   * @param array $data
   *   The data array to generate the table from.
   * @param bool $show_relationship_information
   *   Whether to show relationship information.
   *
   * @return array
   *   The render array for the table.
   */
  public function generateEntityBasicInfoTable(array $data, bool $show_relationship_information = FALSE) {
    $header = [
      ['data' => $this->t('Name')],
      ['data' => $this->t('Machine Name')],
      ['data' => $this->t('Bundle')],
      ['data' => $this->t('Bundle Machine Name')],
      ['data' => $this->t('Title Label')],
      ['data' => $this->t('Edit Own')],
      ['data' => $this->t('Edit Any')],
      ['data' => $this->t('Total Count')],
    ];

    if ($show_relationship_information) {
      $header[] = ['data' => $this->t('Referenced By')];
    }

    $row_data = [
      htmlspecialchars($data['entity_name'] ?? ''),
      htmlspecialchars($data['entity_machine_name'] ?? ''),
      htmlspecialchars($data['bundle_name'] ?? ''),
      htmlspecialchars($data['bundle_machine_name'] ?? ''),
      htmlspecialchars($data['title_field_label'] ?? ''),
      htmlspecialchars(implode(', ', $data['edit_own_access']) ?? ''),
      htmlspecialchars(implode(', ', $data['edit_any_access']) ?? ''),
      htmlspecialchars($data['total_count'] ?? ''),
    ];

    if ($show_relationship_information) {
      $row_data[] = new FormattableMarkup($data['reference_info'] ?? '', []);
    }

    $rows = [
      ['data' => $row_data],
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => [
          'entity-basic-info-table',
          'structure-map-table',
        ],
      ],
    ];

    return $build;
  }

  /**
   * Generates a table from the provided basic field entity info array.
   *
   * @param array $data
   *   The basic field entity info array to generate the table from.
   *
   * @return array
   *   The render array for the table.
   */
  public function generateBasicFieldEntityInfoTable(array $data) {
    $header = [
      ['data' => $this->t('Label')],
      ['data' => $this->t('Machine Name')],
      ['data' => $this->t('Field Type')],
      ['data' => $this->t('Required')],
      ['data' => $this->t('Cardinality')],
      ['data' => $this->t('Default value')],
      ['data' => $this->t('Settings')],
    ];

    $rows = [];

    foreach ($data as $field_info) {
      $default_value = $field_info['default_value'];

      // Check if the field is an entity reference.
      if (is_array($default_value) && isset($field_info['field_type']) && $field_info['field_type'] === 'entity_reference') {
        $entity_labels = [];
        foreach ($default_value as $entity_id) {
          $entity = \Drupal::entityTypeManager()->getStorage('entity_type')->load($entity_id);
          if ($entity) {
            $entity_labels[] = $entity->label();
          }
        }
        $default_value = implode(', ', $entity_labels);
      }
      else {
        if (is_array($default_value) && isset($default_value[0]['value'])) {
          if (!empty($default_value)) {
            $default_value = $default_value[0]['value'];
          }
          else {
            $default_value = '';
          }
        }
        else {
          $default_value = $default_value;
        }
      }
      if (is_array($default_value)) {
        $default_value = '';
      }

      $rows[] = [
        'data' => [
          htmlspecialchars($field_info['label'] ?? ''),
          htmlspecialchars($field_info['machine_name'] ?? ''),
          htmlspecialchars($field_info['field_type'] ?? ''),
          htmlspecialchars($field_info['required'] ?? ''),
          htmlspecialchars($field_info['cardinality'] ?? ''),
          htmlspecialchars($default_value ?? ''),
          new FormattableMarkup($field_info['settings'] ?? '', []),
        ],
      ];
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => [
          'basic-field-entity-info-table',
          'structure-map-table',
        ],
      ],
    ];

    return $build;
  }

  /**
   * Generates a table for form display modes using Drupal's render arrays.
   *
   * @param array $data
   *   The data array containing form display modes.
   * @param bool $show_hidden_fields
   *   Whether to show hidden fields.
   *
   * @return array
   *   The render array for the tables.
   */
  public function generateFormDisplayTables(array $data, bool $show_hidden_fields = FALSE) {
    $build = [];

    foreach ($data as $display_mode => $info) {
      $header = [
        ['data' => $this->t('Status')],
        ['data' => $this->t('Label')],
        ['data' => $this->t('Machine Name')],
        ['data' => $this->t('Field Group')],
        ['data' => $this->t('Widget Label')],
        ['data' => $this->t('Widget Machine Name')],
        ['data' => $this->t('Widget Configuration')],
      ];

      $rows = [];
      if (!empty($info['enabled'])) {
        $rows[] = [
          'data' => [
            [
              'data' => $this->t('Enabled'),
              'rowspan' => count($info['enabled']) + 1,
            ],
          ],
        ];
        foreach ($info['enabled'] as $field) {
          $rows[] = [
            'data' => [
              htmlspecialchars($field['field_label'] ?? ''),
              htmlspecialchars($field['machine_name'] ?? ''),
              htmlspecialchars($field['field_group'] ?? ''),
              htmlspecialchars($field['widget_label'] ?? ''),
              htmlspecialchars($field['widget_name'] ?? ''),
              new FormattableMarkup($field['widget_configuration'] ?? '', []),
            ],
          ];
        }
      }
      if ($show_hidden_fields) {
        if (!empty($info['disabled'])) {
          $rows[] = [
            'data' => [
              [
                'data' => $this->t('Disabled'),
                'rowspan' => count($info['disabled']) + 1,
              ],
            ],
          ];
          foreach ($info['disabled'] as $field) {
            $rows[] = [
              'data' => [
                htmlspecialchars($field['field_label'] ?? ''),
                htmlspecialchars($field['machine_name'] ?? ''),
              ],
            ];
          }
        }
      }

      $build['table_' . $display_mode] = [
        '#type' => 'table',
        '#prefix' => '<h4>' . $this->t('Display Mode: @label', ['@label' => htmlspecialchars($info['label'])]) . '</h4>',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => [
          'class' => [
            'form-display-table',
            'structure-map-table',
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * Generates a table for view display modes using Drupal's render arrays.
   *
   * @param array $data
   *   The data array containing view display modes.
   * @param bool $show_hidden_fields
   *   Whether to show hidden fields.
   *
   * @return array
   *   The render array for the tables.
   */
  public function generateViewDisplayTables(array $data, bool $show_hidden_fields = FALSE) {
    $build = [];

    foreach ($data as $display_mode => $info) {
      $header = [
        ['data' => $this->t('Status')],
        ['data' => $this->t('Label')],
        ['data' => $this->t('Machine Name')],
        ['data' => $this->t('Label Display')],
        ['data' => $this->t('Format')],
        ['data' => $this->t('Format Settings')],
      ];

      $rows = [];
      if (!empty($info['enabled'])) {
        $rows[] = [
          'data' => [
            [
              'data' => $this->t('Enabled'),
              'rowspan' => count($info['enabled']) + 1,
            ],
          ],
        ];
        foreach ($info['enabled'] as $field) {
          $rows[] = [
            'data' => [
              htmlspecialchars($field['field_label'] ?? ''),
              htmlspecialchars($field['machine_name'] ?? ''),
              htmlspecialchars($field['label_settings'] ?? ''),
              htmlspecialchars($field['format'] ?? ''),
              new FormattableMarkup($field['format_settings'] ?? '', []),
            ],
          ];
        }
      }

      if ($show_hidden_fields) {
        if (!empty($info['disabled'])) {
          $rows[] = [
            'data' => [
              [
                'data' => $this->t('Disabled'),
                'rowspan' => count($info['disabled']) + 1,
              ],
            ],
          ];
          foreach ($info['disabled'] as $field) {
            $rows[] = [
              'data' => [
                htmlspecialchars($field['field_label'] ?? ''),
                htmlspecialchars($field['machine_name'] ?? ''),
              ],
            ];
          }
        }
      }

      $build['table_' . $display_mode] = [
        '#type' => 'table',
        '#prefix' => '<h4>' . $this->t('Display Mode: @label', ['@label' => htmlspecialchars($info['label'])]) . '</h4>',
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => [
          'class' => [
            'view-display-table',
            'structure-map-table',
          ],
        ],
      ];
    }

    return $build;
  }

}
